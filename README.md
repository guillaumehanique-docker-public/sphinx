# ghanique/sphinx

## Introduction

### What is Sphinx?

[sphinx](http://www.sphinx-doc.org/en/master/) is a compiler for documentation.
The source code is `reStructuredText`.
It can compile into html-pages, single-html, various help-formats, various e-book formats, latex, pdf, plain text and man-pages.
(I haven't tested if this docker image can do all of them).

### Why this image?

It took me too much effort to install it.
And then I had to reinstall my OS and I had to do it all over again.
The near-perfect solution to make this repeatable is to create a docker-image that can compile documents.

## Usage

### Make

This repository comes with a `Makefile` that does two things:

1. Build the image
2. Run the image

It has three constants:

| Name       | Value    | Description |
|------------|----------|-------------|
| MAINTAINER | ghanique | That would be me :-). It also becomes part of the image name. |
| IMAGENAME  | sphinx   | The name of the docker image. |
| VERSION    | 0.1.0    | The version of the docker image. |

### Building the image

You can build the image by executing `make build`.

When you build the image the image gets two tags (using the example above):

* `ghanique/sphinx:latest` and
* `ghanique/sphinx:0.1.0`

Notice that the image is quite large (several GB) due to all the fonts that are installed.

### Running the image

Running the image using this `Makefile` is mostly for debugging purposes.
It will create the container, give you a terminal and delete the image when you're done.
With `args=` you can tell it what to do.

I.e.:

`make run args=/bin/bash`

will give you a container with a bash-prompt.
If you leave the container it will be deleted.

## To do

Right now it's an image that has the `sphinx-build` command (probably amoung others).
It should bind to some volume so that it can compile documentation.
It should also get some simple interface to make it compile to the various output files.

## Won't do

I haven't tested if it correctly compiles to all potential output formats.
I'll test it for the formats I use most.

When it compiles the document it creates an out-directory.
All those files will be owned by `root` as that is the user that is executing the build in the container.
So after you use this container to build the documentation, you'll have to change ownership of the build output yourself.
