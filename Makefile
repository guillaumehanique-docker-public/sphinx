MAINTAINER=ghanique
IMAGENAME=sphinx
VERSION=0.1.0

.PHONY: help
help:
	@echo "Please use any of the following targets:"
	@echo "    build        Build the dockerfile. Tag it with '${MAINTAINER}/${IMAGENAME}'."
	@echo "    run args=    Run the image '${MAINTAINER}/${IMAGENAME}' in a container."
	@echo "                 The args will be added to de run command."
	@echo "    runbash      Run container and start a bash shell."

.PHONY: build
build:
	docker build -t ${MAINTAINER}/${IMAGENAME} -t ${MAINTAINER}/${IMAGENAME}:${VERSION} .

.PHONY: run
run:
	docker run -ti --rm \
	-v ${CURDIR}/source:/source \
	-v ${CURDIR}/build:/build \
	-e "SOURCE=" \
	${MAINTAINER}/${IMAGENAME} ${args}

.PHONY: runbash
runbash:
	docker run -ti --rm \
	-v ${CURDIR}/source:/source \
	-v ${CURDIR}/build:/build \
	-e "SOURCE=" \
	--entrypoint=/bin/bash \
	${MAINTAINER}/${IMAGENAME}
