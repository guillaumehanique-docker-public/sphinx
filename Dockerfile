FROM python:3.8.3-buster
LABEL maintainer="guillaume@hanique.com"

COPY files/* /
RUN pip install -r requirements.txt

VOLUME [ "/source" ]
VOLUME [ "/build" ]

ENTRYPOINT [ "make" ]
